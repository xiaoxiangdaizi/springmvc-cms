#springmvc-cms

##SpringMVC 项目的演示案例

###技术组成

* Spring
* SpringMVC
* Spring-JDBC
* Freemarker
* Bootstrap
* Maven
* Druid


###功能介绍

* 新增
* 修改
* 删除/批量删除
* 批量共享/取消共享
* 导出excel
