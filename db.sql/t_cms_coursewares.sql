/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50096
Source Host           : 127.0.0.1:3306
Source Database       : cms

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2016-05-17 22:55:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_cms_coursewares`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_coursewares`;
CREATE TABLE `t_cms_coursewares` (
  `id` varchar(32) NOT NULL,
  `code` varchar(20) default NULL,
  `name` varchar(20) default NULL,
  `duration` int(10) default NULL,
  `device_type` varchar(20) default NULL,
  `file_type` varchar(20) default NULL,
  `open` varchar(1) default NULL,
  `transform_status` varchar(2) default NULL,
  `create_time` datetime default NULL,
  `update_time` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_cms_coursewares
-- ----------------------------
INSERT INTO `t_cms_coursewares` VALUES ('0d2d1206c58c47b5a7b037edd9674249', '9', '9', '9', 'PC', 'SCORM包', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('1665f48839d7453db1b76b911dc09000', '2', '2', '2', 'PC,MOBILE', '视频', '1', '1', null, null);
INSERT INTO `t_cms_coursewares` VALUES ('1afd690916ae471fa6968d1ea3a10703', '11', '11', '11', 'PC', 'SCORM包', '1', '0', null, null);
INSERT INTO `t_cms_coursewares` VALUES ('26da3625d0cc4991afd5828b7dd71466', 'test6', 'test6', '16', 'PC,MOBILE', '视频', '1', '0', '2016-05-17 22:44:12', '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('32b5b5bbea7244ecb40fa4dd215c8b15', 'test3', 'test3', '13', 'PC', 'SCORM包', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('573e78a21f2641d0ba60518c98b0ba44', '7', '7', '7', 'PC', 'SCORM包', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('57fbaeb776224c48ab25cf15944aa789', '5', '5', '5', 'PC', 'SCORM包', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('95187e00f7ac44d3a4c1237cbe9dabf2', '112', '12', '12', 'PC', 'SCORM包', '1', '1', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('d309002c024a4353a91ba98043787599', 'test5', 'test5', '15', 'PC,MOBILE', 'SCORM包', '1', '0', '2016-05-17 22:43:41', '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('d897aaf7c96844e98b2558df12f351c2', '3', '3', '3', 'PC,MOBILE', '录播课程', '1', '1', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('e740d626327141de8c5796bb23a8b60a', '6', '6', '6', 'PC', 'SCORM包', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('fbf6799527b844dabcc9828d890d7581', 'test', 'test', '11', 'PC,MOBILE', '视频', '1', '0', null, '2016-05-17 22:54:32');
INSERT INTO `t_cms_coursewares` VALUES ('ff6d662e633c42329ae6273566df7e46', '10', '10', '10', 'PC', '文档', '1', '0', null, null);
