import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Created by liguoqing on 2015/12/17.
 */
public class AppTest {

    public static void main(String[] args) {

        Server jettyServer = new Server(8088);
        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setContextPath("/cms");  // 启动时url地址栏输入的工程名
        //webAppContext.setDescriptor("/src/main/webapp/WEB-INF/web.xml");
        //webAppContext.setResourceBase("/src/main/webapp");
        webAppContext.setParentLoaderPriority(true);
        //1、以模块的形式存在：cms/src/main/webapp
        //2、以项目的形式存在：src/main/webapp
        webAppContext.setWar("src/main/webapp");
        jettyServer.setHandler(webAppContext);
        try {
            jettyServer.start();
            //jettyServer.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
